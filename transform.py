from argparse import ArgumentParser
import os

import numpy as np
import cv2

from _fisheye_transform import FisheyeTransform, methods
from _image_widget import ImageWidget
import skimage.transform


def transform(image, x, y, f, alpha, tform_method):
    max_dim = max(image.shape)
    pad_sizes = [((max_dim - s) // 2, (max_dim - s) // 2) if i < 2 else (0, 0) for i, s in
                 enumerate(image.shape)]
    image = np.pad(image, pad_sizes, 'reflect')
    x += pad_sizes[1][0]
    y += pad_sizes[0][0]
    tform = FisheyeTransform(f, alpha, mapping_function=tform_method, center=(x, y))
    return skimage.transform.warp(image, tform, mode="reflect", preserve_range=True,
                                  output_shape=(alpha, alpha)).round().astype(image.dtype)


# parsing arguments
parser = ArgumentParser()
parser.add_argument("src_folder", help="source folder containing the images and positions.csv")
parser.add_argument("dst_folder", nargs="?", help="path to the output directory. Defaults to [src_folder]/transformed")
parser.add_argument("-f", "--focal_length", help="focal length")
parser.add_argument("-a", "--alpha", help="size of the selected area")
parser.add_argument("-m", "--mapping_fn", choices=methods, default="equidistant",
                    help="mapping function for the fisheye transform")
args = parser.parse_args()
src_folder = args.src_folder
dst_folder = args.dst_folder if args.dst_folder is not None else os.path.join(src_folder, "transformed")
show_gui = None in [args.focal_length, args.alpha]  # if either f or alpha is missing, the gui will open
run_transform = not show_gui

distortion_params = {
    "f": int(args.focal_length) if args.focal_length is not None else 1000,
    "alpha": int(args.alpha) if args.alpha is not None else 100,
    "method": args.mapping_fn
}

# initializing image metadata
filenames = np.genfromtxt(os.path.join(src_folder, "positions.csv"), str, delimiter=",", usecols=[0])
x = np.genfromtxt(os.path.join(src_folder, "positions.csv"), delimiter=",", usecols=[1])
y = np.genfromtxt(os.path.join(src_folder, "positions.csv"), delimiter=",", usecols=[2])
filenames = list(map(lambda fn: os.path.join(src_folder, fn), filter(lambda fn: fn != "positions.csv", filenames)))
if show_gui:
    from PyQt5.QtGui import QPixmap, QImage
    from PyQt5.QtWidgets import QApplication, QLabel, QWidget, QSlider, QVBoxLayout, QHBoxLayout, QSizePolicy, \
        QPushButton
    from PyQt5.QtCore import Qt
    from tqdm import tqdm
    # store parameters of current object
    file_idx = 0
    current_obj = {
        "file_idx": file_idx,
        "filename": filenames[file_idx],
        "x": x[file_idx],
        "y": y[file_idx],
        "image": cv2.imread(filenames[file_idx])
    }

    def update_image():
        """updates the image on parameter change"""
        dst = transform(
            current_obj["image"],
            current_obj["x"],
            current_obj["y"],
            distortion_params["f"],
            distortion_params["alpha"],
            distortion_params["method"]
        ).astype(current_obj["image"].dtype)
        dst = cv2.resize(dst, (512, 512), interpolation=cv2.INTER_LANCZOS4)
        dst = cv2.cvtColor(dst, cv2.COLOR_BGR2RGB)
        image_label.setPixmap(QPixmap.fromImage(QImage(dst.data, dst.shape[1], dst.shape[0], dst.shape[1]*3, QImage.Format_RGB888)))

    def update_name():
        """Updates the name of the currently displayed image"""
        current_obj["filename"] = os.path.join(src_folder, filenames[current_obj["file_idx"]])
        current_obj["x"] = x[current_obj["file_idx"]]
        current_obj["y"] = y[current_obj["file_idx"]]
        current_obj["image"] = cv2.imread(current_obj["filename"])
        image_name_label.setText("Object #%d (%s)" % (current_obj["file_idx"], os.path.basename(current_obj["filename"])))

    # callbacks for changing parameters
    def f_cb(val):
        distortion_params["f"] = val
        f_val_label.setText("%s" % val)
        update_image()

    def alpha_cb(val):
        distortion_params["alpha"] = val
        alpha_val_label.setText("%s" % val)
        update_image()

    def method_cb(val):
        distortion_params["method"] = methods[val]
        method_val_label.setText(distortion_params["method"])
        update_image()

    def cell_cb(val):
        current_obj["filename"] = os.path.join(src_folder, filenames[val])
        current_obj["x"] = x[val]
        current_obj["y"] = y[val]
        current_obj["image"] = cv2.imread(current_obj["filename"])
        # im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        update_image()

    def next_cb(_):
        current_obj["file_idx"] += 1
        if current_obj["file_idx"] >= len(filenames):
            current_obj["file_idx"] = 0
        update_name()
        update_image()

    def prev_cb(_):
        current_obj["file_idx"] -= 1
        if current_obj["file_idx"] < 0:
            current_obj["file_idx"] = len(filenames)-1
        update_name()
        update_image()

    def transform_cb(val):
        global run_transform
        run_transform = True
        app.closeAllWindows()

    def add_slider(min_val, max_val, value, label, callback):
        """adds a new slider for changing the value of the given parameter"""
        slider_layout = QHBoxLayout()
        slider = QSlider(Qt.Horizontal)
        slider.setMaximum(max_val)
        slider.setMinimum(min_val)
        slider.setValue(value)
        slider.valueChanged.connect(callback)
        slider_label = QLabel("%s:" % label)
        slider_label.setFixedSize(80, 20)
        slider_layout.addWidget(slider_label)
        slider_layout.addWidget(slider)
        val_label = QLabel("%s" % slider.value())
        val_label.setFixedSize(80, 20)
        slider_layout.addWidget(val_label)
        slider_widget = QWidget()
        slider_widget.setLayout(slider_layout)
        slider_widget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        layout.addWidget(slider_widget)
        return val_label

    # opening the gui
    app = QApplication([])
    w = QWidget()
    layout = QVBoxLayout()
    w.setGeometry(100, 100, 200, 50)
    # sliders for the distortion parameters
    alpha_val_label = add_slider(1, 512, distortion_params["alpha"], "alpha", alpha_cb)
    f_val_label = add_slider(1, 2000, distortion_params["f"], "f", f_cb)
    method_val_label = add_slider(0, len(methods) - 1, methods.index(distortion_params["method"]), "method", method_cb)

    # panel for changing the distorted object
    navigation_box = QHBoxLayout()
    prev_btn = QPushButton("<")
    prev_btn.setFixedSize(30, 30)
    prev_btn.clicked.connect(prev_cb)
    navigation_box.addWidget(prev_btn)
    image_name_label = QLabel()
    update_name()
    image_name_label.setAlignment(Qt.AlignCenter)
    navigation_box.addWidget(image_name_label)
    next_btn = QPushButton(">")
    next_btn.setFixedSize(30, 30)
    next_btn.clicked.connect(next_cb)
    navigation_box.addWidget(next_btn)
    layout.addLayout(navigation_box)

    # image display
    image_label = ImageWidget()
    image_label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
    image_label.setAlignment(Qt.AlignCenter)
    image_layout = QHBoxLayout()
    image_layout.addWidget(image_label, Qt.AlignHCenter)
    image_widget = QWidget()
    image_widget.setLayout(image_layout)
    image_widget.setMinimumSize(512, 512)
    layout.addWidget(image_widget)

    transform_btn = QPushButton("Transform dataset")
    transform_btn.clicked.connect(transform_cb)
    layout.addWidget(transform_btn)

    w.setLayout(layout)
    w.setWindowTitle("Fisheye transform")
    w.show()
    method_cb(methods.index(distortion_params["method"]))
    code = app.exec_()
if not run_transform:
    # if the gui was closed, the transformation will not run
    exit()
print("Exporting images to %s" % dst_folder)
if os.path.exists(dst_folder):
    if len(os.listdir(dst_folder)) > 0:
        print(
            "Output directory %s exists and is not empty. Please select an empty/non-existent folder!" %
            dst_folder
        )
        exit()
else:
    os.mkdir(dst_folder)

for fn, xp, yp in tqdm(list(zip(filenames, x, y))):
    im = cv2.imread(fn)
    out_fn = os.path.join(dst_folder, os.path.basename(fn))
    i = 0
    while os.path.exists(out_fn):
        i += 1
        name, ext = os.path.basename(fn).split(".")
        out_fn = os.path.join(dst_folder, "%s_%d.%s" % (name, i, ext))
    tformed = transform(im, xp, yp, distortion_params["f"], distortion_params["alpha"], distortion_params["method"])
    cv2.imwrite(out_fn, tformed)
with open(os.path.join(dst_folder, "transform_params.txt"), "w") as log_file:
    log_file.writelines(("%s: %s\n" % (param_name, param_value) for param_name, param_value in distortion_params.items()))
