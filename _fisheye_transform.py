import numpy as np
from typing import Optional, Union, Tuple, Callable
from numbers import Number

methods = ["rectilinear", "stereographic", "equidistant", "equisolid", "orthographic"]


class FisheyeTransform:
    """A callable class executing fisheye transformation."""

    r_f_2_theta_maps = {
        "rectilinear": lambda r, f: np.arctan(r/f),
        "stereographic": lambda r, f: 2 * np.arctan(r/(2*f)),
        "equidistant": lambda r, f: r/f,
        "equisolid": lambda r, f: 2 * np.arcsin(r/(2*f)),
        "orthographic": lambda r, f: np.arcsin(r/f)
    }

    def rs_rectilinear(self, r, s):
        return np.arctan(r/(self.ratio * s))

    def rs_stereographic(self, r, s):
        return 2 * np.arctan(r / (2 * self.ratio * s))

    def rs_equidistant(self, r, s):
        return r/(self.ratio * s)

    def rs_equisolid(self, r, s):
        return 2 * np.arcsin(r / (2 * self.ratio * s))

    def rs_orthographic(self, r, s):
        return np.arcsin(r/(self.ratio * s))

    def __init__(
            self, f: Optional[Number] = None, alpha: Optional[Number] = None, d: Optional[Number] = None,
            ratio: Optional[Number] = None, mapping_function: Union[str, Callable] = "equidistant",
            center: Optional[Tuple[Number, Number]] = None, out_size: Optional[Number] = None):
        """
        Description

        Parameters
        ----------
        f
            focal length of the lens. Only one of f and ratio should be provided.
        alpha
            size of the object of interest (in pixels)
        d
            (virtual) distance between the lens and the input points
        ratio
            the ratio between f:s (where s is the output size of the image). Only one of f and ratio should be provided.
        mapping_function
            the mapping function used for the transformation. Either a callable or the name of a mapping function.
             Possible mapping functions:<br>
            <ul style="list-style-type:none;">
             <li><b>rectilinear:</b> <i style="font-family:Calibri; font-size:120%">r = f tan(Θ)</i></li>
             <li><b>stereographic:</b> <i style="font-family:Calibri; font-size:120%">r = 2 f tan(Θ/2)</i></li>
             <li><b>equidistance:</b> <i style="font-family:Calibri; font-size:120%">r = f Θ</i></li>
             <li><b>equisolid:</b> <i style="font-family:Calibri; font-size:120%">r = 2 f sin(Θ/2)</i></li>
             <li><b>orthographic:</b> <i style="font-family:Calibri; font-size:120%">r = f sin(Θ)</i></li>
            </ul>
        center
            the center of the area to transform. If not provided, the center of the image will be used.
        out_size
            The size of the output image. If not provided, it will be the same as the input size.
        """
        if d is not None and ratio is not None:
            pass  # Only one of 'd' and 'ratio' should be provided
        if ratio is not None or f is not None:
            pass  # Only one of 'f' and 'ratio' should be provided
        if alpha is not None and d is not None:
            pass  # Only one of 'd' and 'alpha' should be provided
        if not (ratio or f or d):
            pass  # One of 'f', 'd' or 'ratio' should be provided
        if f is not None and alpha is None and d is None:
            pass # If 'f' is provided, one of 'alpha' or 'd' should also be provided
        if ratio is not None and alpha is None:
            pass # If 'ratio' is provided, 'alpha' should also be provided
        if alpha is not None and hasattr(alpha, "__getitem__") and len(alpha) > 2:
            pass # 'alpha' should be of length 2 (or a single number)
        self.f = f
        self.ratio = ratio
        self.src_diagonal_length = None if alpha is None\
            else alpha*np.sqrt(2) if type(alpha) == int\
            else np.sqrt(alpha[0]**2 + alpha[1]**2)
        self.r_f_2_theta = FisheyeTransform.r_f_2_theta_maps[mapping_function] if type(mapping_function) is str else mapping_function
        self.center = center
        self.out_size = out_size
        self.d = d

    def __call__(self, points):
        pts_mean = points.mean(0, keepdims=True)
        center = self.center if self.center is not None else pts_mean
        s = self.out_size or points.max()
        center = np.asarray(center).reshape(1, -1)
        f = s * self.ratio if self.ratio else self.f
        r = np.sqrt(np.sum(np.square(points - pts_mean), 1))
        r_max = s / 2 * np.sqrt(2)
        theta = self.r_f_2_theta(r, f)
        theta_max = self.r_f_2_theta(r_max, f)
        d = self.src_diagonal_length / 2 / (np.tan(theta_max) + 10e-7) if self.d is None else self.d
        p = (d * np.tan(theta).reshape(-1, 1))
        transformed = (points - pts_mean) * p / (r.reshape(-1, 1)+10e-7) + center
        return transformed
