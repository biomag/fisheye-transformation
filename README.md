# Fisheye-Transform

## Installation
Clone the repository with
```
git clone https://bitbucket.org/biomag/fisheye-transformation.git
```
It is advised to create a virtual environment
```
python -m virtualenv venv
source venv/bin/activate # activation on Linux
venv\Scripts\activate.bat # activation on Windows
```

You can install the required packages from the repository folder with
```
pip install -r requirements.txt
```

## Usage
The application can be run from console:
```
python transform.py
  PATH/TO/INPUT_IMAGES
  [PATH/TO/DESTINATION]
  [-f FOCAL_LENGTH]
  [-a ALPHA]
  [-m MAPPING_FUNCTION]
```
### Parameters
* `PATH/TO/INPUT_IMAGES`: path to the folder containing the input images for the transformation. 
  The folder should also include a text file called `positions.csv`, which contains the 
  center positions of the object selected for transformation. Format:
  ```
  filename1,x_position1,y_position1
  filename2,x_position2,y_position2
  ...
  ```
* `PATH/TO/DESTINATION`: the target folder for the transformed images. If not provided, images 
  will be saved to `path/to/input_images/transformed`
* `FOCAL_LENGTH`: the focal length of the virtual fisheye lens. If missing, a GUI will appear
  for experimenting with different values.
* `ALPHA`: size of the transformed area in pixels. If missing, a GUI will appear for 
  experimenting with different values.
* `MAPPING_FUNCTION`: mapping function of the virtual fisheye lens.
  Must be one of `rectilinear`, `stereographic`, `equidistant`, `equisolid` or `orthographic`.
  Defaults to `equidistant`.<br>
  <i>Note: The FisheyeTransform class can also use a callable, which takes two parameters,
  the radius of the transformed point and the focal length. This allows for virtually any
  custom mapping function to be used with the transformation.</i>
