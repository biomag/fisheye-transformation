from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QLabel


class ImageWidget(QLabel):
    def __init__(self, *args, **kwargs):
        self.img = None
        super().__init__(*args, **kwargs)

    def setPixmap(self, a0: QtGui.QPixmap) -> None:
        self.img = a0
        super().setPixmap(a0)
        self.resize_pixmap()

    def resizeEvent(self, a0: QtCore.QSize) -> None:
        self.resize_pixmap()

    def resize_pixmap(self):
        if self.img is not None:
            resized_pixmap = self.img.scaled(self.width(), self.height(), QtCore.Qt.KeepAspectRatio)
            super().setPixmap(resized_pixmap)